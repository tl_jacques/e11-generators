# Não altere a linha seguinte
import time

# Altere a linha seguinte adicionando zeros conforme o video do enunciado
MAX_RANGE = 10000000

def exercise_1(max_range):
    list_comprehension =[i for i in range(max_range)]
    return list_comprehension


def exercise_1_yield(max_range):
    for number in range(max_range):
        yield number


# Nao altere a partir dessa linha para baixo !!

# Sem YIELD
start = time.process_time()

exercise_1(MAX_RANGE)

end = time.process_time() - start
print(f"-- Tempo de execução SEM YIELD: {end:.6f} segundos --")


# Com YIELD
start = time.process_time()

exercise_1_yield(MAX_RANGE)

end = time.process_time() - start
print(f"-- Tempo de execução COM YIELD: {end:.6f} segundos --")